import mutations from './mut_client'

export const store = {
  state: () => ({
    clients: []
  }),
  mutations
}

export default store