import Vuex from 'vuex'
import client from './client'

new Vuex.Store({
  namespaced: true,
  state: {
    client
  }
})

export const strict = false