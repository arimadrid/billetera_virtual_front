# ePayco

## Descripción

> Repositorio front-end para proyecto de simulación de billetera virtual que permite realizar las operaciones de:
### Registro de clientes:
Ingresando su documento, nombres teléfono y email.
### Recargar saldo a su billetera.
Mediante el uso de su documento, teléfono y el monto que desea recargar.
### Consultar saldo.
Colocando su documento y teléfono el sistema le indica el saldo actual.
### Pagar compras.
Realice sus pagos ingresando su documento, télefono y el monto a pagar. El sistema le enviará un codigo de confirmación a su correo electrónico, el cual deberá ingresar en la ventana emergente que muestra a página para poder confirmar su pago.

## Para usar el sistema siga los siguientes pasos:

> Clone los repositorios, tanto front-end como back-end.
> Cree el archivo .env tal y como se indica en el env.example
> Para usar el front-end instale las dependencias de la siguiente manera:

## Build Setup

```bash

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
